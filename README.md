<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Satti test

После загрузки проекта:

Docker:
- cd ~/satti
- cp .env.example .env
- composer install
- docker-compose build app
- docker-compose up -d
- docker-compose exec app php artisan migrate:fresh --seed(Необязательно)
- docker-compose exec app php artisan config:clear
- docker-compose exec app php artisan key:generate
- [localhost:8000/api/order](localhost:8000/api/order) GET.

## Комментарии разработчика: 
При получение заказов притягиваются данные их точек, в этом же моменте притягиваются поля с Google Directions.

P.S.: Все координаты одинаковы так как fake()->latitude(),  fake()->longitude() формирируют некорректные данные... 

[https://gitlab.com/Sarsebek/satti-logistics](https://gitlab.com/Sarsebek/satti-logistics)