<?php

namespace App\Http\Resources;

use App\Services\GoogleDirectionsService;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderPointResource extends JsonResource
{
    private $statuses = [
        1 => 'new',
    ];

    // Дистанция
    private $distance;

    // Длительность езды
    private $duration;

    /**
     * Формируем наш получаемый JSON.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // получаем данные с GoogleDirections
        $this->getDD();

        return [
            'order_id' => $this->order_id,
            'type' => $this->type,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'status' => $this->statuses[$this->status],
            'distance' => $this->distance,
            'duration' => $this->duration,
        ];
    }

    /** Метод вызова нашего компонента Google Directions
     * @return void
     */
    private function getDD()
    {
        $data = (new GoogleDirectionsService($this->latitude, $this->longitude))->make();

        $this->distance = $data['distance'];
        $this->duration = $data['duration'];
    }
}
