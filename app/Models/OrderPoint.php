<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPoint extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'type',
        'latitude',
        'longitude',
        'status',
    ];

    // убираем даты, так как все они создаются вместе с заказом
    public $timestamps = false;

}
