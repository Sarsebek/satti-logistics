<?php

namespace App\Services;

use TeamPickr\DistanceMatrix\DistanceMatrix;
use TeamPickr\DistanceMatrix\Licenses\StandardLicense;

class GoogleDirectionsService
{

    //
    private $latitude;

    //
    private $longitude;

    /**
     * Конструктор класса
     * @param $latitude
     * @param $longitude
     */
    public function __construct($latitude, $longitude)
    {

        // инициализация
        $this->latitude = $latitude;

        // инициализация
        $this->longitude = $longitude;

    }

    public function make() : array
    {
        // получаем лицензию через наш ключ
        $license = new StandardLicense(env('GOOGLE_MAPS_KEY'));

        // ответ от нашего сервиса
        $response = DistanceMatrix::license($license)
            ->addOrigin('astana, kz')
            ->addDestination($this->latitude. ', ' .$this->longitude)
            ->request();

        // получение данных немного страшна, но рабочее
        // можно было и через curl, но так посчитал удобнее
        $rows = $response->rows();
        $elements = $rows[0]->elements();
        $element = $elements[0];

        // получаем данные в двух форматах
        // в нашем случае текстовое
        $distance = $element->distance();
        $distanceText = $element->distanceText();
        $duration = $element->duration();
        $durationText = $element->durationText();

        return [
            'distance' => $distanceText,
            'duration' => $durationText,
        ];

    }

}
