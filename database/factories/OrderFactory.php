<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Статика наших фейк данных.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'number' => fake()->unique()->numberBetween(1,1000),
            'status' => 1,
        ];
    }
}
