<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderPoint>
 */
class OrderPointFactory extends Factory
{
    /**
     * Статика наших фейк данных.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'type' => fake()->numberBetween(1,2),
            'latitude' => '52.603669',
            'longitude' => '1.223785',
            'status' => 1,
        ];
    }
}
