<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_points', function (Blueprint $table) {
            $table->id();

            $table->foreignId('order_id')->constrained()->comment('идентификатор заказа');
            $table->enum('type', [1,2])->default(2)->comment('1 это точка сбора, 2 точка клиента');
            $table->double('latitude')->comment('долгота');
            $table->double('longitude')->comment('широта');
            $table->integer('status')->default(1)->comment('статус');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_points');
    }
};
