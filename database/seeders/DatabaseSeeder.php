<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\OrderPoint;
use Illuminate\Database\Seeder;
use App\Models\Order;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Создание заказов
         Order::factory(10)->has(
             // Создание точек заказа
             OrderPoint::factory()->count(3), 'points'
         )->create();

    }
}
