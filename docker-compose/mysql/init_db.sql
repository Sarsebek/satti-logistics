# ************************************************************
# Sequel Pro SQL dump
# Version 5438
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.28-0ubuntu0.20.04.3)
# Database: satti
# Generation Time: 2022-10-04 17:46:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2019_08_19_000000_create_failed_jobs_table',1),
	(4,'2019_12_14_000001_create_personal_access_tokens_table',1),
	(5,'2022_10_03_115053_create_orders_table',1),
	(6,'2022_10_03_115129_create_order_points_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table order_points
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_points`;

CREATE TABLE `order_points` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint unsigned NOT NULL,
  `type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '1 это точка сбора, 2 точка клиента',
  `latitude` double NOT NULL COMMENT 'долгота',
  `longitude` double NOT NULL COMMENT 'широта',
  `status` int NOT NULL DEFAULT '1' COMMENT 'статус',
  PRIMARY KEY (`id`),
  KEY `order_points_order_id_foreign` (`order_id`),
  CONSTRAINT `order_points_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `order_points` WRITE;
/*!40000 ALTER TABLE `order_points` DISABLE KEYS */;

INSERT INTO `order_points` (`id`, `order_id`, `type`, `latitude`, `longitude`, `status`)
VALUES
	(1,1,'1',52.603669,1.223785,1),
	(2,1,'1',52.603669,1.223785,1),
	(3,1,'1',52.603669,1.223785,1),
	(4,2,'1',52.603669,1.223785,1),
	(5,2,'1',52.603669,1.223785,1),
	(6,2,'2',52.603669,1.223785,1),
	(7,3,'1',52.603669,1.223785,1),
	(8,3,'1',52.603669,1.223785,1),
	(9,3,'2',52.603669,1.223785,1),
	(10,4,'1',52.603669,1.223785,1),
	(11,4,'2',52.603669,1.223785,1),
	(12,4,'2',52.603669,1.223785,1),
	(13,5,'2',52.603669,1.223785,1),
	(14,5,'1',52.603669,1.223785,1),
	(15,5,'1',52.603669,1.223785,1),
	(16,6,'1',52.603669,1.223785,1),
	(17,6,'2',52.603669,1.223785,1),
	(18,6,'2',52.603669,1.223785,1),
	(19,7,'1',52.603669,1.223785,1),
	(20,7,'2',52.603669,1.223785,1),
	(21,7,'1',52.603669,1.223785,1),
	(22,8,'1',52.603669,1.223785,1),
	(23,8,'1',52.603669,1.223785,1),
	(24,8,'1',52.603669,1.223785,1),
	(25,9,'1',52.603669,1.223785,1),
	(26,9,'2',52.603669,1.223785,1),
	(27,9,'1',52.603669,1.223785,1),
	(28,10,'1',52.603669,1.223785,1),
	(29,10,'2',52.603669,1.223785,1),
	(30,10,'1',52.603669,1.223785,1);

/*!40000 ALTER TABLE `order_points` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `number` int NOT NULL COMMENT 'номер заказа',
  `status` int NOT NULL DEFAULT '1' COMMENT 'статус заказа',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_number_unique` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `number`, `status`, `created_at`, `updated_at`)
VALUES
	(1,191,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(2,548,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(3,616,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(4,147,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(5,921,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(6,288,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(7,354,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(8,30,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(9,942,1,'2022-10-04 17:45:49','2022-10-04 17:45:49'),
	(10,908,1,'2022-10-04 17:45:49','2022-10-04 17:45:49');

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table personal_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `personal_access_tokens`;

CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
